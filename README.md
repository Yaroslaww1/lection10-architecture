# README #

Chosen architecture - **microservices with event bus**. Why? There are some benefits over monolithic in a context of our application:

* We already have explicit domains like registration, bar, payments...
* However, some of these domains will probably change a lot, and we cant predict how
* We know that number of users will increase and at some point in the future, so we must think about scalability of our application

## List of microservices ##
* Users (login, registration, roles)
* Bar (assortment, orders)
* Payment (payments, promo codes...)
* Facilities (rent, buy, sell)
* Trainings
* Blog
* Notifications
* Achievements

Note: of course, some of microservices are too heavy, so many of them (probably all) will divide into smaller microservices in the future.

## Technologies ##
* http - robust solution as we don't have fast changed data and high traffic
* web sockets - if we need data subscriptions (menu updating for example) we can use ws
* Nodejs
* React
* RabbitMQ - event bus