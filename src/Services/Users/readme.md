# Microservice #

This folder will contain all source files for one microservice. It uses Anemic Domain Model.

### Technologies: ###
* nodejs 
* express
* socket.io
* typeorm
  
### layers of microservice ###
1. presentation - /api and /events and /socket
2. business - /services
3. persistance - /data/repositories 
4. database - /data/db