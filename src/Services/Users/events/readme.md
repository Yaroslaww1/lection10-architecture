# Service events #

As we use event bus to communicate between microservices, this folder will contain:
* handlers for some specific inbound events from other microservices. For example: achievements microservice will track all events and add achievement to user if some rules are completed.
* outbound events.
